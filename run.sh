#!/bin/bash

trap "exit" INT
set -e

. script/parameters.sh

# Benchmark variables
export bin_path=$PWD/benchmark
export polybench=$bin_path/polybench
export mibench=$bin_path/mibench
export iot_locus=$bin_path/iot-locus

# Check if there is an already defined $results variable. If not, define a
# default results dir. The $results variable points to the folder that stores
# the simulations results
if [[ -z "$results" ]]; then
    export results=$PWD/results
fi

export simulations=$results/simulations
export traces=$results/traces
mkdir -p $traces/polybench
mkdir -p $traces/mibench
mkdir -p $traces/iot-locus

# Simulate polybench applications with gem5
simulate_polybench() {
    # Big
    for i in $(ls $bin_path/polybench/neon/*.x); do
        base_name=$(basename -s .x $i)
        out_dir=$simulations/polybench/big/$base_name
        gem5_big_sim "$out_dir" "$i"
    done

    # Little
    for i in $(ls $bin_path/polybench/neon/*.x); do
        base_name=$(basename -s .x $i)
        out_dir=$simulations/polybench/little/$base_name
        gem5_little_sim "$out_dir" "$i"
    done
}

# $1: runme.txt of the benchmark
mibench_parse_name() {
    bench_path=$(dirname $1)
    if grep -q '#' $1; then
        bench_name=$(sed 's/.*#//' $1)
    else
        bench_name=$(basename $bench_path)
    fi
    printf "$bench_name"
}

# Return the binary name used by given runme
# $1: runme.txt of the benchmark
mibench_parse_prog() {
    prog=$(cut -d' ' -f1 $1).x
    printf "$prog"
}

# Return the binary name used by given runme
# $1: runme.txt of the benchmark
mibench_parse_dump() {
    dump=$(cut -d' ' -f1 $1).dump
    # This is a workaraound to deal with binaries that are not in the root of
    # its own benchmark folder, e.g., jpeg benchmark.
    dump=$(basename $dump)
    printf "$dump"
}

# Return the command (binary and arguments)
# $1: runme.txt of the benchmark
mibench_parse_cmd() {
    executable=$(mibench_parse_prog $1)
    # Get only the arguments of the runme command
    args=$(sed 's/[^ ]* //' $1)
    # Remove meta comment, if any
    args=$(echo $args | sed 's/#.*//')
    cmd="$executable $args"
    printf "$cmd"
}

# $1: runme.txt of the benchmark
# $2: 'big' or 'little' switch
mibench_parse_runme() {
    bench_path=$(dirname $1)
    bench_name=$(mibench_parse_name $1)
    cmd=$(mibench_parse_cmd $1)

    printf "(cd $bench_path && "
    if [ "$2" = 'big' ]; then
        link_cmd=" && ln -sf $1 $simulations/mibench/big/$bench_name/runme.txt)"
        gem5_big_sim "$simulations/mibench/big/$bench_name" "$cmd" "$link_cmd"
    elif [ "$2" = 'little' ]; then
        gem5_little_sim "$simulations/mibench/little/$bench_name" "$cmd" ")"
    fi
}
export -f mibench_parse_runme

# Simulate mibench applications with gem5
simulate_mibench() {
    for i in $(ls -d $mibench/*/*/runme_small*.txt); do
        mibench_parse_runme "$i" 'big'
    done

    for i in $(ls -d $mibench/*/*/runme_small*.txt); do
        mibench_parse_runme "$i" 'little'
    done
}

# Simulate iot-locus applications with gem5
simulate_iot_locus() {
    # Big
    for i in $(find $iot_locus/testbench -type f -name "*.x"); do
        base_name=$(basename -s .x $i)
        out_dir=$simulations/iot-locus/big/$base_name
        bench_path=$(dirname $i)
        printf "(cd $bench_path && "
        gem5_big_sim "$out_dir" "$i" ")"
    done

    # Little
    for i in $(find $iot_locus/testbench -type f -name "*.x"); do
        base_name=$(basename -s .x $i)
        out_dir=$simulations/iot-locus/little/$base_name
        bench_path=$(dirname $i)
        printf "(cd $bench_path && "
        gem5_little_sim "$out_dir" "$i" ")"
    done
}
# End

# Generate traces for the benchmark applications supported
generate_traces() {
    # Polybench
    for i in $(ls -d $simulations/polybench/big/*); do
        bench_name=$(basename $i)
        commits_file="$i/commit-trace"
        dump_file="$bin_path/polybench/neon/$bench_name.dump"
        out_trace="$traces/polybench/$bench_name.trace"
        make_trace "$commits_file" "$dump_file" "$out_trace"
    done

    # Mibench
    for i in $(ls -d $simulations/mibench/big/*); do
        bench_name=$(basename $i)
        commits_file="$i/commit-trace"
        dump_link=$(readlink $i/runme.txt)
        dump_dir=$mibench/dump
        dump_file=$dump_dir/$(mibench_parse_dump $dump_link)
        out_trace="$traces/mibench/$bench_name.trace"
        make_trace $commits_file $dump_file $out_trace
    done

    # iot-locus
    for i in $(ls -d $simulations/iot-locus/big/*); do
        bench_name=$(basename $i)
        commits_file="$i/commit-trace"
        dump_file="$iot_locus/dump/$bench_name.dump"
        out_trace="$traces/iot-locus/$bench_name.trace"
        make_trace "$commits_file" "$dump_file" "$out_trace"
    done
}

simulate_offloader_polybench() {
    for i in $(ls -d $traces/polybench/*); do
        name=$(basename -s .trace $i)
        printf "$SIMULATOR -d $results/sim-neon/polybench/$name \
            --big-thread $i $results/simulations/polybench/little/$name\n"
    done
}

simulate_offloader_mibench() {
    for i in $(ls -d $traces/mibench/*); do
        name=$(basename -s .trace $i)
        printf "$SIMULATOR -d $results/sim-neon/mibench/$name \
            --big-thread $i $results/simulations/polybench/little/$name\n"
    done
}
