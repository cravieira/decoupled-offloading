# Decoupled Offloader

This repository implements the decoupled offloader simulator for Asymmetric Multi-Core (AMC) processors. The work is based on the ARM's big.LITTLE design and its NEON ISA.

### Dependencies

#### Gem5

This project depends on the gem5 simulator to produce the traces used as input to the decoupled offloader simulator. You must build the gem5 for the ARM ISA before using this simulator. Please refer to the gem5 for detailed build instructions.


#### GNU Parallel

This repository relies on automation scritps that launches jobs in parallel using [GNU parallel](https://www.gnu.org/software/parallel/). This allow

```
sudo apt install parallel
```

### Benchmarks

### Simulation Flow

1. Simulate a benchmark with gem5 to generate commit and issue traces.
2. Preprocess the commit trace to look for instruction synchronizations and prepare the trace used in the simulator.
3. Use the processed commit trace and the issue trace to simulate the offloader.

### Automation

This repository provides scripts to automate the simulation flow. Use the *simulate_<benchmark_name>()* functions to simulate the available benchmarks and generate the necessary traces. The results are stored at the path defined by the *$results* variable in [run.sh](./run.sh). After the gem5 simulations, it is necessary to process the generated commit traces to create the traces used by the decoupled offloader simulator.

Finally, the simulator can be used with the *simulate_offloader_<benchmark_name>* to simulate single-thread scenarios. In this case, the big core offloads NEON instructions to an idle little core.

##### Example usage

All functions available in [run.sh](./run.sh) only print the commands to the terminal. You can execute the commands individually or use the GNU parallel utility. We provide an parallel wrapper function (*exec_cmd_list*) to execute jobs in parallel.

```bash
exec_cmd_list "$(simulate_polybench)" # Simulate polybench with Gem5
exec_cmd_list "$(simulate_mibench)" # Simulate mibench with Gem5
exec_cmd_list "$(generate_traces)" # Process traces to the simulator
exec_cmd_list "$(simulate_offloader_polybench)" # Simulate the offloader-polybench
exec_cmd_list "$(simulate_offloader_mibench)" # Simulate the offloader-polybench
```

After appending one of these commands to run.sh, execute the script

```
./run.sh
```

### Benchmarks

We provide [benchmarks](./benchmark) from different sources: polybench, mibench, and iot-locus. You can either compile the benchmarks using the [build.sh](./benchmark/build.sh) script or using the already compiled binaries. Please refer to the benchmark instructions in the benchmark folder.

### Quickstart

1. Build [gem5](#Gem5).
2. Install GNU [parallel](#GNU-parallel).
3. Generate traces and run the offloader simulator.

```
cat >> run.sh << 'EOF'
exec_cmd_list "$(simulate_polybench)" # Simulate polybench with Gem5
exec_cmd_list "$(generate_traces)" # Process traces to the simulator
exec_cmd_list "$(simulate_offloader_polybench)" # Simulate the offloader-polybench
EOF
```

