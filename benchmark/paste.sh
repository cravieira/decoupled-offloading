#!/bin/bash

set -e

# Install binaries and dump files into a benchmark folder
# $1: Source folder
# $2: Target benchmark folder
install_benchmarks() {
    local wd=$PWD

    cd $1
    for i in $(find $2 -name '*.x'); do
        mkdir -p $wd/$(dirname $i)
        cp $i $wd/$i
    done
    for i in $(find $2 -name '*.dump'); do
        mkdir -p $wd/$(dirname $i)
        cp $i $wd/$i
    done
    cd $wd
}

paste_benchmarks() {
    tar -xvf binaries.tar.gz
    install_benchmarks binaries polybench
    install_benchmarks binaries mibench
    install_benchmarks binaries iot-locus
}

paste_benchmarks
