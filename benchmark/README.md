# Benchmarks

This folder contains the benchmarks used to evaluate the decoupled offloader.

### Build

To build the benchmarks, you must have the *arm-linux-gnueabihf-gcc* toolchain in your path. In this case, just execute the build script

```
./build.sh
```

If you use the ARM toolchain available in Ubuntu 20>, it will probably fail when simulating with gem5. The reason is that this toolchain uses a syscall that is not implemented in the gem5 version used in this work.

### Precompiled Binaries

You can also use the precompiled binaries to execute your simulations. Just run the `paste.sh` script to copy the benchmarks from the *binaries* folder to their right places.

```
./paste.sh
```
