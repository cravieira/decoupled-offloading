#!/bin/bash

set -e

# Copy binaries and dump files from the benchmark folders to a new path
# $1: New folder
# $2: Benchmark folder
copy_benchmark_folder() {
    for i in $(find $2 -name '*.x'); do
        dir="$1/"$(dirname "$i")
        mkdir -p $dir
        cp $i $dir"/"$(basename "$i")
    done
    for i in $(find $2 -name '*.dump'); do
        dir="$1/"$(dirname "$i")
        mkdir -p $dir
        cp $i $dir"/"$(basename "$i")
    done
}

get_benchmarks() {
    mkdir -p "$1"
    copy_benchmark_folder "$1" polybench
    copy_benchmark_folder "$1" mibench
    copy_benchmark_folder "$1" iot-locus
}

copy_benchmarks
