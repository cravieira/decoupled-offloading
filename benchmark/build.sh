#!/bin/bash

set -e

(cd polybench && ./build.sh)
(cd mibench && ./build.sh)
(cd iot-locus && ./build.sh)
