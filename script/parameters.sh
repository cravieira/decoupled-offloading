#!/bin/bash

export PROCS=$(nproc)

export GEM5="$PWD/gem5"
export G5_EXE="$GEM5/build/ARM/gem5.opt"
big_dflags="CommitTrace"
little_dflags="MinorFUIssue,MinorCommitTrace"
export G5_DFLAGS="--debug-flags=$big_dflags,$little_dflags --debug-file=debug"
export G5_CONFIG="$GEM5/configs/example/arm/se_bigLITTLE.py"
export G5_BIG_ARGS="--big-cores 1"
export G5_LITTLE_ARGS="--little-cores 1"

export TRACER="$PWD/src/tracer.py"
export SIMULATOR="$PWD/src/simulate.py"

# $1: List of commands to be run
exec_cmd_list() {
    printf "$1" | parallel -j$PROCS --halt now,fail=1
}

# Print gem5 command for big simulation
# $1: The output directory of the simulation
# $2: The binary and its arguments
# $3: Optional: Command to be appended
gem5_big_sim() {
    # Run gem5
    sim_cmd="$G5_EXE $G5_DFLAGS -re -d $1 $G5_CONFIG $G5_BIG_ARGS '"$2"'"
    # Remove useless chars from the trace
    cmd1="sed 's/:.*://' $1/debug > $1/commit-trace"
    # Append "last tick" to the generated commit-trace file
    cmd2="tail -n 1 $1/simout >> $1/commit-trace"
    # Remove useless debug file
    rm_cmd="rm $1/debug"
    printf "$sim_cmd && $cmd1 && $cmd2 && $rm_cmd$3\n"
}

# Print gem5 command for little simulation and make the "issue-trace" file
# $1: The output directory of the simulation
# $2: The binary and its arguments
# $3: Optional: Command to be appended
gem5_little_sim() {
    # Run gem5
    sim_cmd="$G5_EXE $G5_DFLAGS -re -d $1 $G5_CONFIG $G5_LITTLE_ARGS '"$2"'"
    # Grep only issues to NEON and mem FUs
    cmd1=$'grep \'fu: [45]\''" $1/debug"
    # Remove useless words from gem5 debug report
    cmd2="awk "$'\'{print $1, $4, $5}\''
    # Remove useless ":" char and generate a simpler issue trace file
    cmd3="sed 's/://' > $1/issue-trace"
    # Append "last tick" to the generated issue-trace file
    cmd4="tail -n 1 $1/simout >> $1/issue-trace"
    # Generate issue-trace file
    issue_trace="$cmd1 | $cmd2 | $cmd3 && $cmd4"

    # Grep commit
    cmd1="grep \'MinorCommitTrace\' $1/debug"
    # Remove "svc" and "translation faults" commits. "svc" is never committed
    # by the O3 model commit trace, and I did not manage to remove
    # "translation faults" in gem5 from the MinorCommitTrace.
    cmd2="grep -v \'svc\|translation\'"
    # Get only the tick column
    cmd3="awk "$'\'{print $1}\''
    # Remove useless ":" char
    cmd4="sed 's/://' > $1/commit-trace"
    # Append "last tick"
    cmd5="tail -n 1 $1/simout >> $1/commit-trace"
    # Generate commit-trace file
    commit_trace="$cmd1 | $cmd2 | $cmd3 | $cmd4 && $cmd5"
    # Remove useless debug file
    rm_cmd="rm $1/debug"

    printf "$sim_cmd && $issue_trace && $commit_trace && $rm_cmd$3\n"
}

# Call tracer script to generate analyze gem5 commits file
# $1: Commits file
# $2: Binary dump
# $3: Generated trace file path
make_trace() {
    printf "$TRACER $1 $2 > $3\n"
}
