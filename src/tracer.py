#!/usr/bin/env python3

import sys

from util.parsers import findViolations

if (len(sys.argv) > 1):
    findViolations(sys.argv[1], sys.argv[2])
else:
    print("No argument given!")
