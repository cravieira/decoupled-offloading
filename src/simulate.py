#!/usr/bin/env python3

import sys
import argparse
import inspect
import json

from sim.sim import Sim

import sim.arbiter
import sim.scheduler

# Get all non abstract classes to make them available as options
arbiters = [m[0] for m in inspect.getmembers(sim.arbiter, inspect.isclass)
        if m[1].__module__ == 'sim.arbiter' and not m[0].startswith('Base')]
schedulers = [m[0] for m in inspect.getmembers(sim.scheduler, inspect.isclass)
        if m[1].__module__ == 'sim.scheduler' and not m[0].startswith('Base')]

def assert_parameters(args, parser):
    bigs = len(args.big_thread)
    littles = len(args.little_thread)
    no_clusters = bigs if littles > littles else littles

    # Check if the number of partial clusters is valid
    msg = 'Number of partial clusters is greater than the number of clusters'
    if args.partial_clusters > no_clusters:
        parser.error(msg)

parser = argparse.ArgumentParser(description='Simulate a trace in remote\
        datapath.')
# Thread options
parser.add_argument('--big-thread', nargs=2, action='append', default=[], help='Big and little gem5 simulation output directory')
parser.add_argument('--little-thread', nargs=2, action='append', default=[], help='Big and little gem5 simulation output directory')
parser.add_argument('--batch-file', help='Json file containing batches of threads to be executed. This file allows complex simulations parameters to be passed')

group = parser.add_mutually_exclusive_group()
group.add_argument('--sim-insts', type=int , help='Number of instructions simulated by each thread. If not given, each thread is run to completion. The trace file can be read multiple times if the number of instructions specified in this argument is bigger than the thread trace')
group.add_argument('--run-for', type=int, help='Simulate all threads for the given number of cycles. The trace file is repeated if the number of cycles is bigger than the trace')

# Datapath options
parser.add_argument('--little-queues-size', type=int, default=32, help='Input queues size in little core')
parser.add_argument('--big-queues-size', type=int, default=16, help='Output queues size in big core')
parser.add_argument('--memqueue-size', type=int, default=16, help='Mem queue size')
parser.add_argument('--network-delay', type=int, default=0, help='Number of cycles necessary to send data between cores')
parser.add_argument('--switch-overhead', type=int, default=200, help='Number of cycles necessary to switch between modes')

# Arbiter options
parser.add_argument('--arbiter', choices=arbiters, help='Use an arbiter among the options')
parser.add_argument('--arbiter-interval', type=int, default=1000, help='Number of cycles a predict is requested')
parser.add_argument('--arbiter-to-big', type=int, default=200, help='Max overhead in cycles to change simulation mode from remote to big')
parser.add_argument('--arbiter-to-remote', type=float, default=0.2, help='Minimal rate of NEON instructions commit to change simulation mode from big to remote')

# Cluster options
parser.add_argument('--partial-clusters', type=int, default=0, help='Sets the number of partial clusters, which are clusters that only work in offload mode')

# Scheduler options
parser.add_argument('--scheduler', choices=schedulers, help='Use a scheduler among the options')
parser.add_argument('--os-call-interval', type=int, default=10000, help='Number of cycles the OS is called, i.e., the OS is called every \'os-call-interval\' cycles')
parser.add_argument('--os-overhead', type=int, default=500, help='Minimal number of cycles a thread spend in OS mode after entering it')
parser.add_argument('--os-migration-overhead', type=int, default=5000, help='Number of cycles spent to migrate a thread between cores')

# General simulation options
parser.add_argument('--ticks-to-clock', type=int, default=500, help='Conversion number from gem5 ticks to cpu clock')
parser.add_argument('-d', '--output', type=str, default='simout', help='Set simulation output directory')

args = parser.parse_args()
#assert_parameters(args, parser)

batch = None
if args.batch_file is not None:
    with open(args.batch_file, 'r') as f:
        batch = json.load(f)

sim = Sim(args, batch)
print("Starting simulation...", file=sys.stderr)
sim.run()
sim.writeStatsFile()
