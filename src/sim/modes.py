from enum import Enum

class ArbiterModes(Enum):
    CHANGING = 0
    REMOTE = 1
    BIG = 2

class ThreadModes(Enum):
    LITTLE = 0
    BIG = 1
    MIGRATION = 2

class PrivilegeModes(Enum):
    USER = 0
    OS = 1
