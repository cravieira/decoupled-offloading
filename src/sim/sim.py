import sys
import pathlib
from importlib import import_module

from sim.arbiter import *
from sim.cluster import Cluster
from sim.scheduler import *
from sim.stage import *
from sim.batch import BatchManager
from sim.thread import Thread
from sim.modes import ThreadModes

class Sim:
    """"""
    def __init__(self, args, batch=None):
        self._finished = False
        self._ticks_to_clk = args.ticks_to_clock

        self._bm = None
        if batch is None:
            self.clusters = self._makeClustersFromArgs(args)
        else:
            self._bm = BatchManager(batch, args)
            self.clusters = self._bm._makeClustersFromBatches(args)

        self._setArbiter(args)
        self._setScheduler(args)
        if self._bm is not None:
            self._bm.setScheduler(self.scheduler)

        self._max_cycles = args.run_for

        # Create output directory
        self._output = args.output
        pathlib.Path(self._output).mkdir(parents=True, exist_ok=True)
        with open(self._output+'/arguments.txt', 'w') as f:
            print(*sys.argv, file=f)

    def _makeClustersFromArgs(self, args):
        bigs = len(args.big_thread)
        littles = len(args.little_thread)
        no_clusters = bigs if bigs > littles else littles

        clusters = []
        for i in range(no_clusters):
            big_thread = None
            little_thread = None
            if i < len(args.big_thread):
                big_thread = Thread(args.big_thread[i],
                        os_call_interval=args.os_call_interval,
                        sim_insts=args.sim_insts,
                        run_for=args.run_for,
                        ticks_to_clock=args.ticks_to_clock,
                        mode=ThreadModes.BIG)
            if i < len(args.little_thread):
                little_thread = Thread(args.little_thread[i],
                        os_call_interval=args.os_call_interval,
                        sim_insts=args.sim_insts,
                        run_for=args.run_for,
                        ticks_to_clock=args.ticks_to_clock,
                        mode=ThreadModes.LITTLE)
            clusters.append(Cluster(args, big_thread, little_thread))

        return clusters

    def _setArbiter(self, args):
        Arbiter = None
        if args.arbiter:
            Arbiter = getattr(import_module('sim.arbiter'), args.arbiter)

        for i in range(len(self.clusters)):
            if i < len(self.clusters) - args.partial_clusters and\
                    Arbiter is not None:
                arb = Arbiter(args.arbiter_interval,
                        args.arbiter_to_big,
                        args.arbiter_to_remote)
                self.clusters[i].setArbiter(arb)
            else:
                self.clusters[i].setArbiter(None)

    def _setScheduler(self, args):
        self.scheduler = None
        if args.scheduler is not None:
            Scheduler = getattr(import_module('sim.scheduler'), args.scheduler)
            self.scheduler = Scheduler(args, self.clusters, self._bm)

    def run(self):
        cycles = 0
        while not self._finished:
            for c in self.clusters:
                c.tick()

            if self.scheduler:
                os_calls = [c.isCallingOs() for c in self.clusters]
                calls = [item for t in os_calls for item in t]
                if any(calls):
                    self.scheduler.eval(os_calls)

            if self._bm is not None:
                self._bm.eval()

            if self._max_cycles:
                self._finished = cycles >= self._max_cycles
                cycles += 1
            else:
                self._finished = all([c.isDone() for c in self.clusters])

    def _initStats(self):
        self._stats = []

    def resetStats(self):
        for s in self._stats:
            s.zero()

    def writeStatsFile(self, stream=None):
        if stream is None:
            f = open(self._output+'/stats.txt', 'w')
        else:
            f = stream
        if self.scheduler is not None:
            self.scheduler.writeStatsFile(f)
        for c in self.clusters:
            c.writeStatsFile(f)

        if self._bm is not None:
            self._bm.writeBatchesFile(self._output)

        if stream is None:
            f.close()
