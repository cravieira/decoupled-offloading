from enum import IntEnum

from util.commit import Commit

# Little functional units according to gem5 big little scripts.
class FunctionalUnits(IntEnum):
    NEON = 4
    MEM = 5

# Instruction classes as provided in gem5.
OpClass = [
    "No_OpClass",
    "IntAlu",
    "IntMult",
    "IntDiv",
    "FloatAdd",
    "FloatCmp",
    "FloatCvt",
    "FloatMult",
    "FloatMultAcc",
    "FloatDiv",
    "FloatMisc",
    "FloatSqrt",
    "SimdAdd",
    "SimdAddAcc",
    "SimdAlu",
    "SimdCmp",
    "SimdCvt",
    "SimdMisc",
    "SimdMult",
    "SimdMultAcc",
    "SimdShift",
    "SimdShiftAcc",
    "SimdDiv",
    "SimdSqrt",
    "SimdFloatAdd",
    "SimdFloatAlu",
    "SimdFloatCmp",
    "SimdFloatCvt",
    "SimdFloatDiv",
    "SimdFloatMisc",
    "SimdFloatMult",
    "SimdFloatMultAcc",
    "SimdFloatSqrt",
    "SimdReduceAdd",
    "SimdReduceAlu",
    "SimdReduceCmp",
    "SimdFloatReduceAdd",
    "SimdFloatReduceCmp",
    "SimdAes",
    "SimdAesMix",
    "SimdSha1Hash",
    "SimdSha1Hash2",
    "SimdSha256Hash",
    "SimdSha256Hash2",
    "SimdShaSigma2",
    "SimdShaSigma3",
    "SimdPredAlu",
    "MemRead",
    "MemWrite",
    "FloatMemRead",
    "FloatMemWrite",
    "IprAccess",
    "InstPrefetch"
]

class OpDesc:

    def __init__(self, opClass, opLat, pipelined=True):
        self.opClass = opClass
        self.opLat = opLat
        self.pipelined = pipelined

    def __str__(self):
        return 'opClass: {}, opLat: {}, pipelined: {}'.format(self.opClass,
                self.opLat,
                self.pipelined)

littleFUPool = [
    OpDesc(opClass='SimdAdd', opLat=6),
    OpDesc(opClass='SimdAddAcc', opLat=4),
    OpDesc(opClass='SimdAlu', opLat=4),
    OpDesc(opClass='SimdCmp', opLat=1),
    OpDesc(opClass='SimdCvt', opLat=3),
    OpDesc(opClass='SimdMisc', opLat=3),
    OpDesc(opClass='SimdMult', opLat=4),
    OpDesc(opClass='SimdMultAcc', opLat=5),
    OpDesc(opClass='SimdShift', opLat=3),
    OpDesc(opClass='SimdShiftAcc', opLat=3),
    OpDesc(opClass='SimdSqrt', opLat=9),
    OpDesc(opClass='SimdFloatAdd', opLat=8),
    OpDesc(opClass='SimdFloatAlu', opLat=6),
    OpDesc(opClass='SimdFloatCmp', opLat=6),
    OpDesc(opClass='SimdFloatCvt', opLat=6),
    OpDesc(opClass='SimdFloatDiv', opLat=20, pipelined=False),
    OpDesc(opClass='SimdFloatMisc', opLat=6),
    OpDesc(opClass='SimdFloatMult', opLat=15),
    OpDesc(opClass='SimdFloatMultAcc',opLat=6),
    OpDesc(opClass='SimdFloatSqrt', opLat=17),
    OpDesc(opClass='FloatAdd', opLat=8),
    OpDesc(opClass='FloatCmp', opLat=6),
    OpDesc(opClass='FloatCvt', opLat=6),
    OpDesc(opClass='FloatDiv', opLat=15, pipelined=False),
    OpDesc(opClass='FloatSqrt', opLat=33),
    OpDesc(opClass='FloatMult', opLat=6),
    OpDesc(opClass='MemRead', opLat=1),
    OpDesc(opClass='MemWrite',opLat=1),
    OpDesc(opClass='FloatMemRead', opLat=1),
    OpDesc(opClass='FloatMemWrite',opLat=1)
]

def lookup_op_desc(op_id):
    op_index =  op_id
    op_name = OpClass[op_index]
    val_found = next(i for i in littleFUPool if i.opClass == op_name)
    return val_found

