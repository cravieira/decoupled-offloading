from itertools import count

from sim.modes import ArbiterModes, ThreadModes, PrivilegeModes
from sim.stat import Stat
from util.tracers import CommitTracer
from util.tracers import IssueTracer
from util.tracers import LittleCommitTracer

class Thread:
    _ids = count(0)

    def __init__(self,
            sim_path,
            os_call_interval=10000,
            sim_insts=None,
            run_for=None,
            ticks_to_clock=500,
            mode=ThreadModes.BIG):
        self._id = next(self._ids)
        self.thread_mode = mode
        self.privilege_mode = PrivilegeModes.USER
        self._ticks_to_clock = ticks_to_clock
        self.uncommitted = None
        self._inst_v_in_flight = None
        self._sim_path = sim_path
        # Big mode variables
        self._big_tracer = CommitTracer(sim_path[0])
        self.last_tick = self._big_tracer.parseLastTick()
        # Little mode variables
        self._issue_tracer = IssueTracer(sim_path[1]+'/issue-trace')
        self._commit_tracer = LittleCommitTracer(sim_path[1]+'/commit-trace')
        #self.last_tick = self._issue_tracer.parseLastTick()
        self._initStats()
        self._os_timer = 0
        self._os_call_interval = os_call_interval
        self._migration_end = 0 # Time of last migration end

        # How should this thread run?
        self._sim_insts = sim_insts # Simulate the given number of insts
        self._infinite = run_for # The simulator decides when to stop
        self._loop_mode = False # Run the trace to completion
        if self._sim_insts is not None or self._infinite is not None:
            self._loop_mode = True # Wrap the trace when it ends

        self._wraps = 0
        self._wrap_commits = 0

    def little_issue(self, dp):
        if self.isDone():
            return

        self._os_timer += 1
        if self.privilege_mode == PrivilegeModes.OS:
            self.wait()
            if self._os_timer >= self.os_overhead:
                self._exitOsMode()
            return

        if dp.canIssue():
            insts = self._getIssues()
            dp.issue(insts)
            self._littleCommit()
            self.advance()
        else:
            self.wait()

    def big_commit(self, dp, mode):
        if self.isDone():
            return

        self._os_timer += 1
        if self.privilege_mode == PrivilegeModes.OS:
            self.wait()
            if self._os_timer >= self.os_overhead:
                self._exitOsMode()
            return

        if mode == ArbiterModes.CHANGING:
            self.wait()
            self._arbiter_migration_overhead.inc()
            return

        if dp.isInstViolationInFlight():
            self.wait()
            self._inst_v_overhead.inc()
            return

        if self.uncommitted is not None:
            commits = self.uncommitted
            self.uncommitted = None
        else:
            commits = self._getCommits()

        if mode == ArbiterModes.REMOTE:
            while commits:
                c = commits[0]
                if c.isNeon():
                    if not dp.canInsert(c):
                        break
                    dp.insertCommit(c)
                    self.committed_neon.inc()
                # If its a main state load or store, look for memory violations
                elif c.load or c.store:
                    if dp._mem_queue:
                        addrs = c.getAccessedBytes()
                        conflicts = dp.getMemConflicts(addrs)
                        if conflicts:
                            # This approach halts the big core for every
                            # address conflict, including a main state and
                            # coprocessor read to the same address, which is
                            # actually not a problem.
                            self._mem_v_overhead.inc()
                            break
                self.committed_insts.inc()
                self._wrap_commits += 1
                commits.pop(0)
                # If the inserted NEON commit was an instruction violation,
                # break
                if c.isInstViolation():
                    # If the violation was not the last commit of the pack,
                    # increase overhead due to instruction violation
                    if commits:
                        self._inst_v_overhead.inc()
                    break
        elif mode == ArbiterModes.BIG:
            while commits:
                c = commits[0]
                if c.isNeon():
                    self.committed_neon.inc()
                self.committed_insts.inc()
                self._wrap_commits += 1
                commits.pop(0)

        if commits:
            self.wait()
            self.uncommitted = commits
        else:
            self.advance()

    def _initStats(self):
        self._stats = []
        self.tick = Stat('ticks',
                'Number of simulated ticks',
                step=self._ticks_to_clock)
        self.simulated_cycles = Stat('currentCycle',
                'Current simulation cycle')
        self.overhead_cycles = Stat('overheadCycles',
                'Number of overhead as cycles')
        self.committed_insts = Stat('committedInsts',
                'Committed instructions')
        self._stats.append(self.tick)
        self._stats.append(self.simulated_cycles)
        self._stats.append(self.overhead_cycles)
        self._stats.append(self.committed_insts)

        self.committed_neon = Stat('committedNeon',
                'Committed NEON instructions')
        self._mem_v_overhead = Stat('memvOverhead',
                'Overhead due to memory violations')
        self._inst_v_overhead = Stat('instvOverhead',
                'Overhead due to instruction violations')
        self._arbiter_migration_overhead = Stat('arbiterMigrationOverhead',
                'Overhead due to arbiter mode migration')
        self._stats.append(self.committed_neon)
        self._stats.append(self._mem_v_overhead)
        self._stats.append(self._inst_v_overhead)
        self._stats.append(self._arbiter_migration_overhead)

        self.big_mode_cycles = Stat('bigModeCycles',
                'Number of cycles spent running in a big core')
        self.little_mode_cycles = Stat('littleModeCycles',
                'Number of cycles spent running in a little core')
        self.migration_mode_cycles = Stat('migrationModeCycles',
                'Number of cycles spent in thread migration')
        self.user_mode_cycles = Stat('userModeCycles',
                'Number of cycles spent in user mode')
        self.os_mode_cycles = Stat('osModeCycles',
                'Number of cycles spent in OS mode')
        self._stats.append(self.big_mode_cycles)
        self._stats.append(self.little_mode_cycles)
        self._stats.append(self.migration_mode_cycles)
        self._stats.append(self.user_mode_cycles)
        self._stats.append(self.os_mode_cycles)

        self._migrations = Stat('threadMigrations',
                'Number of thread migrations')
        self._stats.append(self._migrations)

    def writeStatsFile(self, f, pre=''):
        pre += 'thread'+str(self._id)+'.'

        for s in self._stats:
            print(pre+str(s), file=f)
        original_cycles = self.tick.val / self.tick.step
        overhead = self.overhead_cycles.val
        impact = overhead / original_cycles * 100
        print(pre+'impact:', impact, file=f)
        print(pre+'simPath:', self._sim_path, file=f)

    def _getCommits(self):
        return self._big_tracer.getCommitsAtTick(self.tick.val)

    def _getIssues(self):
        return self._issue_tracer.getIssuesAtTick(self.tick.val)

    def _littleCommit(self):
        commits = self._commit_tracer.countCommitsAtTick(self.tick.val)
        self.committed_insts.inc(commits)
        self._wrap_commits += commits

    def advance(self):
        self.tick.inc()
        if self._loop_mode and self.tick.val >= self.last_tick:
            self._wrapTraces()
        self._updateCounters()

    def wait(self):
        self.overhead_cycles.inc()
        self._updateCounters()

    def _updateCounters(self):
        self.simulated_cycles.inc()
        if self.thread_mode == ThreadModes.BIG:
            self.big_mode_cycles.inc()
        elif self.thread_mode == ThreadModes.LITTLE:
            self.little_mode_cycles.inc()
        elif self.thread_mode == ThreadModes.MIGRATION:
            self.migration_mode_cycles.inc()

        if self.privilege_mode == PrivilegeModes.USER:
            self.user_mode_cycles.inc()
        elif self.privilege_mode == PrivilegeModes.OS:
            self.os_mode_cycles.inc()

    def isDone(self):
        if self._loop_mode == False: # Run once mode
            return self.tick.val >= self.last_tick
        elif self._sim_insts:
            return self.committed_insts.val >= self._sim_insts
        return False # Infinite mode

    def _wrapTraces(self):
        self.tick.zero()
        self._wraps += 1
        self._wrap_commits = 0
        self._big_tracer.resetTrace()
        self._issue_tracer.resetTrace()
        self._commit_tracer.resetTrace()

    def isCallingOs(self):
        if self.privilege_mode == PrivilegeModes.USER:
            return self._os_timer >= self._os_call_interval
        else:
            return False

    def enterOsMode(self, overhead):
        self._os_timer = 0
        self.os_overhead = overhead
        self.privilege_mode = PrivilegeModes.OS
        # Discard uncommitted instructions. They will be executed when the
        # thread exits OS mode.
        self.uncommitted = None

    def _exitOsMode(self):
        self._os_timer = 0
        self.privilege_mode = PrivilegeModes.USER
        if self.thread_mode == ThreadModes.MIGRATION:
            self._endMigration()

    def startMigration(self, next_mode, overhead):
        if self.thread_mode == ThreadModes.MIGRATION:
            raise RuntimeError('Attempt to migrate a thread in migration')
        if next_mode != self.thread_mode:
            self._swapTracer()
        self.next_mode = next_mode
        self.enterOsMode(overhead)
        self.thread_mode = ThreadModes.MIGRATION
        self._migrations.inc()

    def _endMigration(self):
        self.thread_mode = self.next_mode
        self._migration_end = self.simulated_cycles.val

    def cyclesSinceLastMigration(self):
        return self.simulated_cycles.val - self._migration_end

    def _swapTracer(self):
        # t will hold the tick of the commit number X. Since it is committed,
        # we should seek for the commit at time t+step (ticks_to_clock), which
        # is the next commit that can be executed.
        #t = self._ticks_to_clock
        if self.thread_mode == ThreadModes.BIG:
            t = self._commit_tracer.getTickOfCommit(self._wrap_commits)
            self._commit_tracer.seekTick(t)
            self._issue_tracer.seekTick(t)
            self.last_tick = self._issue_tracer.last_tick
        elif self.thread_mode == ThreadModes.LITTLE:
            t = self._big_tracer.getTickOfCommit(self._wrap_commits)
            self._big_tracer.seekTick(t)
            self.last_tick = self._big_tracer.last_tick

        else:
            raise RuntimeError('Unexpected ThreadMode in Thread._swapTracer()')
        self.tick.val = t
