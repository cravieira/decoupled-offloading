from abc import ABC, abstractmethod

from sim.modes import ThreadModes, PrivilegeModes
from sim.stage import Stat
from sim.thread import Thread

class BaseScheduler(ABC):

    def __init__(self, args, clusters):
        self.os_overhead = args.os_overhead
        self.migration_overhead = args.os_migration_overhead
        self.clusters = clusters
        self._initStats()

    @abstractmethod
    def eval(self, os_calls):
        pass

    def _initStats(self):
        self._stats = []

        self._swaps = Stat('threadSwaps', 'Number of thread swaps')

        self._stats.append(self._swaps)

    def writeStatsFile(self, f, pre=''):
        pre += 'scheduler.'

        for s in self._stats:
            print(pre+str(s), file=f)

    def _startMigration(self, t: Thread, next_mode: ThreadModes):
        if t.thread_mode == ThreadModes.MIGRATION:
            raise RuntimeError('Thread migration of a thread already in\
                    migration')
        t.startMigration(next_mode, self.migration_overhead)

    def _swapThreads(self, c0_id, t0, c1_id, t1):
        c0 = self.clusters[c0_id]
        c1 = self.clusters[c1_id]
        temp_0 = c0.little_thread if t0 else c0.big_thread
        temp_1 = c1.little_thread if t1 else c1.big_thread
        next_mode_0 = ThreadModes.LITTLE if t1 else ThreadModes.BIG
        next_mode_1 = ThreadModes.LITTLE if t0 else ThreadModes.BIG

        self._startMigration(temp_0, next_mode_0)
        self._startMigration(temp_1, next_mode_1)

        if t0:
            c0.little_thread = temp_1
        else:
            c0.big_thread = temp_1
        if t1:
            c1.little_thread = temp_0
        else:
            c1.big_thread = temp_0

        self._swaps.inc()

class Basic(BaseScheduler):

    def __init__(self, args, clusters, batch_manager=None):
        super().__init__(args, clusters)

    def eval(self, os_calls):
        i = 0
        while i < len(self.clusters):
            if True == os_calls[i][0]:
                self.clusters[i].big_thread.enterOsMode(self.os_overhead)
            if True == os_calls[i][1]:
                self.clusters[i].little_thread.enterOsMode(self.os_overhead)
            i += 1

class PingPong(BaseScheduler):

    def __init__(self, args, clusters, batch_manager=None):
        for c in clusters:
            if c.big_thread is None or c.little_thread is None:
                raise RuntimeError('Cluster with empty thread')
        return super().__init__(args, clusters)

    def eval(self, os_calls):
        i = 0
        while i < len(self.clusters):
            if True == os_calls[i][0]:
                self._osAttend(i, 0)
            if True == os_calls[i][1]:
                self._osAttend(i, 1)
            i += 1

    def _osAttend(self, c_id, t):
        c = self.clusters[c_id]
        if t:
            if c.little_thread.privilege_mode == PrivilegeModes.USER:
                self._schedule(c_id)
        else:
            if c.big_thread.privilege_mode == PrivilegeModes.USER:
                self._schedule(c_id)

    def _schedule(self, c_id):
        if not self.clusters[c_id].big_thread.isDone() and \
                not self.clusters[c_id].little_thread.isDone():
            self._swapThreads(c_id, 0, c_id, 1)
