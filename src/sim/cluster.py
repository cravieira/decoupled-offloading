import collections
from itertools import count

from sim.modes import ArbiterModes
from sim.arbiter import BaseArbiter
from sim.stage import *
from sim.stat import Stat

class Cluster:
    _ids = count(0)

    def __init__(self,
            args,
            big_thread=None,
            little_thread=None):
        self._id = next(self._ids)
        self.dp = _Datapath(args)

        self.big_thread = None
        self.little_thread = None

        self.big_thread = big_thread
        self.little_thread = little_thread

        self.arbiter = None
        self._switch_overhead = args.switch_overhead
        self.mode = ArbiterModes.REMOTE # Current cluster mode
        self._on_mode_change = False # Is cluster changing its mode?
        self._change_mode_cycles = 0 # Mode change cycle counter

        self._initStats()

    def tick(self):
        if self.isDone():
            return

        if self.little_thread is not None:
            self.little_thread.little_issue(self.dp)

        if self._on_mode_change:
            self._changeMode()
        if self.big_thread:
            self.big_thread.big_commit(self.dp, self.mode)

        self.dp.tick()

        if self.big_thread is not None:
            if self.mode == ArbiterModes.REMOTE:
                self._cycles_on_remote.inc()
            elif self.mode == ArbiterModes.BIG:
                self._cycles_on_big.inc()
            elif self.mode == ArbiterModes.CHANGING:
                self._cycles_on_change.inc()
            if not self.big_thread.isDone():
                self._big_thread_cycles.inc()
        if self.little_thread is not None and not self.little_thread.isDone():
            self._little_thread_cycles.inc()

        if self.arbiter is not None and self.big_thread is not None:
            pred = self.arbiter.predict(self)
            if pred != self.mode:
                self.mode = ArbiterModes.CHANGING
                self._on_mode_change = True
                self._next_mode = pred
                self.mode_changes.inc()

    def _changeMode(self):
        assert self.big_thread is not None, 'Cluster._changeMode should never'
        'be called in a cluster without a big thread running.'

        # Ensure datapath is drained before counter starts
        if not self.dp.isAnyCommitInFlight():
            self._change_mode_cycles += 1

        # Check if the mode change is complete
        if self._change_mode_cycles == self._switch_overhead:
            self.mode = self._next_mode
            self._on_mode_change = False
            self._change_mode_cycles = 0

    def isDone(self):
        done = True
        if self.big_thread:
            done = self.big_thread.isDone()
        if self.little_thread is not None:
            done = self.little_thread.isDone() and done
        return done

    def setArbiter(self, arbiter: BaseArbiter):
        self.arbiter = arbiter
        if self.arbiter is not None:
            self.mode = ArbiterModes.REMOTE

    def isCallingOs(self):
        if self.big_thread:
            big = self.big_thread.isCallingOs()
        else:
            big = False
        if self.little_thread:
            little = self.little_thread.isCallingOs()
        else:
            little = False
        return (big, little)

    def _initStats(self):
        self._stats = []
        self.mode_changes = Stat('modeChanges',
                'Number of mode changes')
        self._cycles_on_big = Stat('cyclesBig',
                'Number of cycles spent running on big mode')
        self._cycles_on_remote = Stat('cyclesRemote',
                'Number of cycles spent running on remote mode')
        self._cycles_on_change = Stat('cyclesChange',
                'Number of cycles spent on mode changes')

        self._stats.append(self.mode_changes)
        self._stats.append(self._cycles_on_big)
        self._stats.append(self._cycles_on_remote)
        self._stats.append(self._cycles_on_change)

        self._big_thread_cycles = Stat('bigThreadCycles',
                'Number of cycles the big thread is active')
        self._little_thread_cycles = Stat('littleThreadCycles',
                'Number of cycles the little thread is active')
        self._stats.append(self._big_thread_cycles)
        self._stats.append(self._little_thread_cycles)

    def writeStatsFile(self, f, pre=''):
        pre += 'cluster'+str(self._id)+'.'
        for s in self._stats:
            print(pre+str(s), file=f)
        self.dp.writeStatsFile(f, pre)
        if self.big_thread:
            self.big_thread.writeStatsFile(f, pre+'bigThread.')
        if self.little_thread:
            self.little_thread.writeStatsFile(f, pre+'littleThread.')

class _Datapath:

    def __init__(self, args):
        self._initStats()
        self._issue_buffer = collections.deque(maxlen=2)
        self._mem_queue_size = args.memqueue_size
        self._mem_queue = collections.deque(maxlen=self._mem_queue_size)

        # Append stages in reversed order. This will simplify the tick().
        print("Allocating stages")
        self._stages = []
        self._stages.append(CommitStage(args.network_delay))
        self._stages.append(NeonStage(self._mem_queue, self._issue_buffer))
        self._stages.append(QueuesStage(args.little_queues_size))
        self._stages.append(DelayStage(args.network_delay))
        self._stages.append(QueuesStage(args.big_queues_size))

        for i in range(len(self._stages)-1, 0, -1):
            print("i:", i, "i_s:", self._stages[i], "i_prev", self._stages[i-1])
            self._stages[i].attachNextStage(self._stages[i-1])

        self._inst_v_in_flight = None

    def tick(self):
        self._dp_cycles.inc()
        for s in self._stages:
            s.tick()

    def canIssue(self):
        # Only push if the issue buffer is empty. Otherwise, it sill has
        # instructions from the previous cycle in it that were unable to issue.
        if len(self._issue_buffer) == 0:
            return True
        else:
            return False

    def issue(self, insts):
        while insts:
            self._issue_buffer.append(insts.pop(0))

    def canInsert(self, commit):
        # If a instruction violation entered the datapath, then check if it is
        # still in flight.
        if self.isInstViolationInFlight():
            return False

        # If it is a NEON mem access, then check if there is space in mem_queue
        if commit.vstore or commit.vload:
            if len(self._mem_queue) >= self._mem_queue_size:
                self._mem_queue_overhead.inc()
                return False

        if not self._stages[-1].canInsert(commit):
            self._queue_overhead.inc()
        return self._stages[-1].canInsert(commit)

    def insertCommit(self, commit):
        if commit.isInstViolation():
            self._inst_v_in_flight = commit
            self._inst_v_committed.inc()

        self._stages[-1].insertCommit(commit)
        if commit.vstore or commit.vload:
            self._mem_queue.append(commit)

    def isCommitInFlight(self, commit_id):
        for s in self._stages:
            if s.isCommitInStage(commit_id):
                return True
        return False

    def isAnyCommitInFlight(self):
        for s in self._stages:
            if len(s.in_flight_commits) > 0:
                return True
        return False

    def commitsInFlight(self):
        acc = 0
        for s in self._stages:
            acc += len(s.in_flight_commits)
        return acc

    def isInstViolationInFlight(self):
        # Have we issued an instruction violation?
        if self._inst_v_in_flight:
            # If so, check if it is still in flight
            if self.isCommitInFlight(self._inst_v_in_flight.id):
                return True
            # Unset control variable if it is not anymore and return False
            else:
                self._inst_v_in_flight = None
        return False

    def getMemConflicts(self, addrs):
        conflics = []
        for c in self._mem_queue:
            c_addrs = c.getAccessedBytes()
            for a in addrs:
                if a in c_addrs:
                    conflics.append(c)
                    break
        return conflics

    def _initStats(self):
        self._stats = []
        self._dp_cycles = Stat('datapathCycles',
                'Total number of cycles the datapath is active')
        self._inst_v_committed = Stat('instvCommitted',
                'Number of instruction violations committed')
        self._queue_overhead = Stat('queueOverhead',
                'Overhead due to queue full')
        self._mem_queue_overhead = Stat('memQueueOverhead',
                'Overhead due to NEON mem queue full')
        self._stats.append(self._dp_cycles)
        self._stats.append(self._queue_overhead)
        self._stats.append(self._inst_v_committed)
        self._stats.append(self._mem_queue_overhead)

    def writeStatsFile(self, f, pre=''):
        pre += 'datapath.'
        for s in self._stats:
            print(pre+str(s), file=f)
        print(pre+str(self._stages[1].big_issue_blocked), file=f)
