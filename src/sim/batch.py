import copy

from typing import Optional
from sim.cluster import Cluster
from sim.modes import ThreadModes
from sim.thread import Thread

class BatchManager():

    def __init__(self, batches, args) -> None:
        self._batch_dict = batches
        self._batches = self._makeThreadsFromBatch(batches, args)
        self._todo_batches = [copy.copy(b) for b in self._batches]

    def setScheduler(self, sch):
        self.scheduler = sch

    def _makeThreadsFromBatch(self, batches, args):
        os_call_interval = args.os_call_interval
        ticks_to_clock = args.ticks_to_clock
        batch_groups = []

        for b in batches:
            batch = []
            if b['mode'] == 'big':
                thread_mode = ThreadModes.BIG
            elif b['mode'] == 'little':
                thread_mode = ThreadModes.LITTLE
            else:
                raise RuntimeError('Unexpected thread mode \"'+b['mode']+'\"in'
                'json file')

            for thread in b['batches']:
                big_trace = thread['big_trace']
                little_trace = thread['little_trace']
                sim_insts = thread['sim_insts']
                batch.append(Thread([big_trace, little_trace],
                    os_call_interval=os_call_interval,
                    sim_insts=sim_insts,
                    run_for=None,
                    ticks_to_clock=ticks_to_clock,
                    mode=thread_mode))

            batch_groups.append(batch)

        return batch_groups

    def _makeClustersFromBatches(self, args):
        bigs = [bg.pop(0) for bg in self._todo_batches
                if bg[0].thread_mode == ThreadModes.BIG]
        littles = [bg.pop(0) for bg in self._todo_batches
                if bg[0].thread_mode == ThreadModes.LITTLE]
        no_clusters = len(bigs) if len(bigs) > len(littles) else len(littles)

        clusters = []
        for i in range(no_clusters):
            big_thread = None
            little_thread = None
            if i < len(bigs):
                big_thread = bigs[i]
            if i < len(littles):
                little_thread = littles[i]
            clusters.append(Cluster(args, big_thread, little_thread))

        self._active_threads = bigs + littles
        self._clusters = clusters
        return clusters

    def _getNextJob(self, t: Thread):
        for i in range(len(self._batches)):
            if t in self._batches[i]:
                if len(self._todo_batches[i]):
                    return self._todo_batches[i].pop(0)
                return None

        assert False, 'Unknown thread object'

    def _setNewJob(self, old_t: Thread, new_t: Optional[Thread]):
        for c in self._clusters:
            if old_t is c.big_thread:
                c.big_thread = new_t
            elif old_t is c.little_thread:
                c.little_thread = new_t

    def eval(self):
        for t in self._active_threads:
            if t.isDone():
                new_t = self._getNextJob(t)
                self._setNewJob(t, new_t)
                self._active_threads.remove(t)
                if new_t is not None:
                    self._active_threads.append(new_t)
                    if self.scheduler is not None:
                        self.scheduler.updateLastCounters()

    def writeBatchesFile(self, out_dir):
        for i in range(len(self._batches)):
            f = open(out_dir+'/batch{}.txt'.format(i), 'w')
            for t in self._batches[i]:
                t.writeStatsFile(f)
            f.close()
