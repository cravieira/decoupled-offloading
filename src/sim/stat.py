
class Stat:

    def __init__(self, key_name, desc, step=1):
        self.key_name = key_name
        self._desc = desc
        self.step = step
        self.val = 0

    def inc(self, times=1):
        self.val += self.step * times

    def dec(self, times=1):
        self.val -= self.step * times

    def zero(self):
        self.val = 0

    def __str__(self):
        return self.key_name+': '+str(self.val)+'\t#'+self._desc
