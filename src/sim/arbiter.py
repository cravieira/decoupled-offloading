from __future__ import annotations
from abc import ABC, abstractmethod
import collections
import typing

from sim.modes import ArbiterModes
if typing.TYPE_CHECKING:
    from sim.cluster import Cluster

class BaseArbiter(ABC):

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def predict(self, cluster: Cluster) -> ArbiterModes:
        pass

class Basic(BaseArbiter):

    # interval: Prediction happens every "interval" cycles
    # threshold_to_big: Whenever overhead is greater than this value, change
    #                  mode to big
    # threshold_to_remote: Whenever the rate of neon instructions is <= to
    #                     value, change mode to remote
    def __init__(self, interval, threshold_to_big, threshold_to_remote):
        super().__init__()
        self.interval = interval
        self._to_big = threshold_to_big
        self._to_remote = threshold_to_remote
        self._t_id = -1 # Current thread being monitored
        self._last_cycle = -1 # Cycle of last window end
        self._last_overhead = -1 # Overhead of the last window
        self.interval_ended = False # Flag to singal an interval end

    def _updateLastCounters(self, t):
        self._t_id = t._id
        self._last_cycle = t.simulated_cycles.val
        self._last_overhead = t.overhead_cycles.val
        self._last_commits = t.committed_insts.val
        self._last_neon_commits = t.committed_neon.val

    def _isThreadChanged(self, c: Cluster):
        return c.big_thread._id != self._t_id

    def _decideNextMode(self, mode, interval_overhead, neon_rate):
        next_mode = mode
        if mode == ArbiterModes.REMOTE:
            if interval_overhead > self._to_big:
                next_mode = ArbiterModes.BIG
            else:
                next_mode = ArbiterModes.REMOTE
        elif mode == ArbiterModes.BIG:
            if (neon_rate <= self._to_remote):
                next_mode = ArbiterModes.REMOTE
            else:
                next_mode = ArbiterModes.BIG
        return next_mode

    def predict(self, cluster: Cluster):
        big_thread = cluster.big_thread
        mode = cluster.mode

        # Behavior when initializing and changing threads
        if self._isThreadChanged(cluster):
            self._updateLastCounters(big_thread)
            return mode

        cycle = big_thread.simulated_cycles.val
        overhead = big_thread.overhead_cycles.val
        commits = big_thread.committed_insts.val
        neon_commits = big_thread.committed_neon.val
        self.interval_ended = (cycle - self._last_cycle) == self.interval

        # Make sure we get all interval ends correctly
        assert (cycle - self._last_cycle) <= self.interval

        # Behavior when chaning modes
        if mode == ArbiterModes.CHANGING:
            self._updateLastCounters(big_thread)
            return mode
        # Behavior at the end of interval
        elif self.interval_ended:
            commits_interval = commits - self._last_commits
            neon_commits_interval = neon_commits - self._last_neon_commits
            try:
                neon_rate = neon_commits_interval / commits_interval
            except ZeroDivisionError:
                neon_rate = 0.0
            interval_overhead = overhead - self._last_overhead
            self._updateLastCounters(big_thread)
            return self._decideNextMode(mode, interval_overhead, neon_rate)
        return mode


class Performance(Basic):

    def __init__(self,
            interval,
            threshold_to_big,
            threshold_to_remote,
            predictions=10,
            changes=3,
            off_cycles=100000):
        self._predictions = collections.deque(maxlen=predictions)
        self._changes = changes
        self._off_cycles = off_cycles
        self._is_off = False
        super().__init__(interval, threshold_to_big, threshold_to_remote)

    def _countModeChanges(self, q):
        if len(q) == 0:
            return 0

        i = 0
        p = q[0]
        changes = 0
        while i < len(q):
            if p is not q[i]:
                changes += 1
            p = q[i]
            i += 1
        return changes

    def predict(self, cluster: Cluster):
        if super()._isThreadChanged(cluster):
            self._predictions.clear()
            self._is_off = False

        next_mode = super().predict(cluster) # Call Static.predict every cycle

        # Behavior when off
        if self._is_off:
            self._timer += 1
            if self._timer == self._off_cycles:
                self._is_off = False
            else:
                return ArbiterModes.BIG

        # Normal behavior
        if self.interval_ended:
            self._predictions.append(next_mode)
            count = self._countModeChanges(self._predictions)
            if count >= self._changes:
                self._predictions.clear()
                self._is_off = True
                self._timer = 0
                next_mode = ArbiterModes.BIG

        return next_mode
