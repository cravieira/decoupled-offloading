import collections
from abc import ABC, abstractmethod

from .fu import *
from .stat import Stat

class Stage(ABC):

    @abstractmethod
    def __init__(self):
        self._next_stage = None
        self.in_flight_commits = []

    @abstractmethod
    def tick(self):
        pass

    @abstractmethod
    def canInsert(self, commit):
        pass

    @abstractmethod
    def insertCommit(self, commit):
        self.in_flight_commits.append(commit)

    def _removeCommit(self, commit):
        self.in_flight_commits.remove(commit)

    def isCommitInStage(self, commit_id):
        for c in self.in_flight_commits:
            if c.id == commit_id:
                return True
        return False

    def attachNextStage(self, stage):
        self._next_stage = stage

    def getInputPort(self):
        return self._inpurtPort

class DelayStage(Stage):

    def __init__(self, delay):
        if delay <= 0:
            print('DelayStage', self, 'behaving as BypassStage')

        self._delay = delay
        self._current_inst = None
        self._timer = -1
        super().__init__()

    def tick(self):
        if self._current_inst is not None:
            if self._timer > 0:
                self._timer -= 1
            if self._timer == 0:
                # Behavior without next stage
                if self._next_stage is None:
                    self._removeCommit(self._current_inst)
                # Behavior with next stage
                elif self._next_stage.canInsert(self._current_inst):
                    self._next_stage.insertCommit(self._current_inst)
                    self._removeCommit(self._current_inst)

    def canInsert(self, commit):
        if self._delay > 0:
            return self._current_inst is None
        # If delay is < 0, then bypass any insert to the next stage
        else:
            return self._next_stage.canInsert(commit)


    def insertCommit(self, commit):
        if self._delay > 0:
            self._current_inst = commit
            self._timer = self._delay
            super().insertCommit(commit)
        # If delay is 0, then bypass any insert to the next stage
        else:
            self._next_stage.insertCommit(commit)

    def _removeCommit(self, commit):
        self._current_inst = None
        super()._removeCommit(commit)

class QueuesStage(Stage):
    """description"""
    def __init__(self, size):
        self._size = size

        self._inst = collections.deque(maxlen=self._size)
        self._data = collections.deque(maxlen=self._size)

        super().__init__()

    def tick(self):
        # If there is no next stage, then dequeue one instruction per tick.
        if self._next_stage is None:
            if len(self._inst) > 0:
                i = self._inst.popleft()
                words = i.offload_words
                for w in range(0, words, 1):
                    self._data.popleft()
                self._removeCommit(i)

        # Dequeue one instruction per cycle if the next stage can receive it.
        else:
            if len(self._inst) > 0:
                if self._next_stage.canInsert(self._inst[0]):
                    i = self._inst.popleft()
                    self._next_stage.insertCommit(i)
                    self._removeDataQueue(i)
                    self._removeCommit(i)

    def canInsert(self, commit):
        # Check if there is room for new commit
        if len(self._inst) >= self._size:
            return False

        # Check if there is room for new commit's data
        words = commit.offload_words
        if (words + len(self._data)) > self._size:
            return False
        return True

    def insertCommit(self, commit):
        # Insert new commit and its data
        self._inst.append(commit)
        self._insertDataQueue(commit)
        super().insertCommit(commit)

    def _insertDataQueue(self, commit):
        if not commit.vstore and not commit.vload:
            words = commit.offload_words
            for w in range(words):
                self._data.append(commit)

    def _removeDataQueue(self, commit):
        if not commit.vstore and not commit.vload:
            words = commit.offload_words
            for w in range(0, words, 1):
                self._data.popleft()

    def _removeCommit(self, commit):
        return super()._removeCommit(commit)

    def attachNextStage(self, stage):
        return super().attachNextStage(stage)

class NeonStage(Stage):

    # This class behaves like the DelayStage, and is used to simulate non-
    # pipelined functional units. It only needs the information (op_id)
    # contained in little issue trace, which is a subset of the information
    # contained in the big's commit traces.
    # This class is defined inside NeonStage since it is not useful anywhere
    # else.
    class _NonPipedFU():

        def __init__(self, op_desc):
            self.op_class = op_desc.opClass
            self.op_lat = op_desc.opLat
            self.op_id = OpClass.index(self.op_class)
            self._current_inst = None
            self._timer = -1

        def tick(self):
            if self._current_inst is not None:
                if self._timer > 0:
                    self._timer -= 1
                if self._timer == 0:
                    self._current_inst = None

        def canInsert(self, op_id):
            return op_id == self.op_id and self._current_inst is None

        def insertInst(self, inst):
            self._current_inst = inst
            self._timer = self.op_lat

    def __init__(self, mem_queue, issue_buffer=None):
        super().__init__()
        self._current_inst = None # Offloaded instruction from big
        self._timer = -1 # Time left to process offloaded instruction
        self._mem_queue = mem_queue
        self._issue_buffer = issue_buffer
        # Have these types of instructions been issued this cycle?
        self._neon_issued = False
        self._mem_issued = False

        self._initStats()

        # Get all non-pipelined FUs in little FU pool
        non_pipelined_op = [f for f in littleFUPool if not f.pipelined]
        self._non_pipelined_fus = []
        for op in non_pipelined_op:
            self._non_pipelined_fus.append(self._NonPipedFU(op))

    def _little_issue(self):
        # Run through all instructions in Little's issue buffer
        failed_to_issue = []
        while len(self._issue_buffer) > 0:
            i = self._issue_buffer.pop()
            if i['fu'] == FunctionalUnits.NEON:
                # This instruction needs a non-pipelined FU?
                if not i['opDesc'].pipelined:
                    # Find its pipeline and try to issue
                    for fu in self._non_pipelined_fus:
                        if fu.canInsert(i['op']):
                            fu.insertInst(i)
                            self._neon_issued = True
                            break
                    continue
                else:
                    # If it can be issued in any pipelined FU, just issue it
                    self._neon_issued = True
                    continue
            # For now, simulate only that one memory instruction can be issued
            # per cycle.
            elif i['fu'] == FunctionalUnits.MEM:
                self._mem_issued = True
            # If we failed to issue, append it to failed issues list
            else:
                failed_to_issue.append(i)

        # If we fail, re-push instructions into issue_buffer. This will
        # indicate to the Datapath the we were unable to issue all instructions
        # at this cycle.
        if failed_to_issue:
            for i in failed_to_issue:
                self._issue_buffer.append(i)

    def tick(self):
        self._neon_issued = False
        self._mem_issued = False

        if self._issue_buffer is not None:
            self._little_issue()

        if self._current_inst is not None:
            if self._timer > 0:
                self._timer -= 1
            if self._timer == 0:
                # Behavior as last stage
                if self._next_stage is None:
                    self._removeCommit(self._current_inst)
                # Behavior with a next stage
                else:
                    if self._next_stage.canInsert(self._current_inst):
                        self._next_stage.insertCommit(self._current_inst)
                        self._removeCommit(self._current_inst)

        for fu in self._non_pipelined_fus:
            fu.tick()

    def canInsert(self, commit):
        if self._current_inst is not None:
            return False

        if commit.isMemAccess():
            self.big_issue_blocked.inc(self._mem_issued)
            return not self._mem_issued
        if not self._neon_issued:
            # Does it need a non-pipelined FU?
            op_desc = lookup_op_desc(commit.op_class)
            if not op_desc.pipelined:
                # If it needs a non-pipelined FU, then return True if we find
                # any fu that accepts the instruction.
                for fu in self._non_pipelined_fus:
                    if fu.canInsert(commit.op_class):
                        return True
                self.big_issue_blocked.inc()
                return False
            return True

        self.big_issue_blocked.inc()
        return False

    def insertCommit(self, commit):
        op = lookup_op_desc(commit.op_class)

        # If the commit needs a non-pipelined FU, find and insert the commit
        # into the FU. This will make the FU unavailable to Little's
        # instruction issues.
        if not op.pipelined:
            for fu in self._non_pipelined_fus:
                if fu.canInsert(commit.op_class):
                    fu.insertInst(commit)

        self._current_inst = commit
        self._timer = op.opLat
        super().insertCommit(commit)

    def _removeCommit(self, commit):
        self._current_inst = None
        if commit.vstore or commit.vload:
            self._mem_queue.pop()
        super()._removeCommit(commit)

    def attachNextStage(self, stage):
        super().attachNextStage(stage)

    def _initStats(self):
        self._stats = []
        self.big_issue_blocked = Stat('bigIssueBlock',
                'Number of cycles the big thread could not issue')

        self._stats.append(self.big_issue_blocked)

class CommitStage(Stage):

    def __init__(self, to_big_latency):
        super().__init__()
        self._current_inst = None
        self._to_big_inst = None
        self._to_big_latency = to_big_latency
        self._to_big_timer = -1

    def tick(self):
        if self._to_big_inst is not None:
            self._to_big_timer -= 1
            if self._to_big_timer == 0:
                self._removeCommit(self._to_big_timer)
                self._to_big_timer = None

        if self._current_inst is not None:
            self._removeCommit(self._current_inst)
            self._current_inst = None

    def canInsert(self, commit):
        return self._current_inst is None

    def insertCommit(self, commit):
        self._current_inst = commit
        super().insertCommit(commit)

    def _removeCommit(self, commit):
        return super()._removeCommit(commit)

    def attachNextStage(self, stage):
        super().attachNextStage(stage)
