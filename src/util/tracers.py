import os
from abc import ABC, abstractmethod

from util.commit import Commit
from sim.fu import lookup_op_desc

class Tracer(ABC):

    @abstractmethod
    def __init__(self, f):
        self._trace_path = f
        self._trace_file = open(f, 'r')
        self._last_read_tick = 0
        self.end_of_tick_entries = False
        self.last_tick = self.parseLastTick()

    def _seekLine(self, line_no):
        self._trace_file.seek(0)

        i = 0
        while i < line_no:
            self._trace_file.readline()
            i += 1

    # Set trace file pointer to the first commit of a given interval
    def seekTick(self, begin):
        self._seekBeginOfTicks()
        # Control variable to mark that the end of commits table was reached
        self.end_of_tick_entries = False
        prev = self._trace_file.tell()
        l = self._trace_file.readline()
        while l:
            words = l.split()
            if int(words[0]) >= begin:
                self._trace_file.seek(prev)
                self._last_read_tick = int(words[0])
                break
            prev = self._trace_file.tell()
            l = self._trace_file.readline()

    @abstractmethod
    def _seekBeginOfTicks(self):
        self._trace_file.seek(0)

    @abstractmethod
    def parseLastTick(self) -> int:
        pass

    def resetTrace(self) -> None:
        self._last_read_tick = 0
        self.end_of_tick_entries = False
        self._seekBeginOfTicks()

class CommitTracer(Tracer):

    def __init__(self, f):
        super().__init__(f)
        # Skip last tick line
        self._trace_file.readline()

    # Set trace file pointer to the first commit of a given interval
    def _seekTraceFile(self, begin):
        # Control variable to mark that the end of commits table was reached
        self.end_of_tick_entries = False
        self._trace_file.seek(0)
        # Skip first line since it contains the "Last tick" and go to the
        # commits part.
        self._trace_file.readline()
        prev = self._trace_file.tell()
        for l in self._trace_file:
            words = l.split()
            if int(words[0]) >= begin:
                self._trace_file.seek(prev)
                self._last_read_tick = int(words[0])
                break
            prev = self._trace_file.tell()

    def _seekBeginOfTicks(self):
        super()._seekBeginOfTicks()
        self._trace_file.readline()

    def getCommitsAtTick(self, tick):
        if self._trace_file.closed:
            self._trace_file = open(self._trace_path, 'r')
            self._trace_file.readline()

        # Avoid unnecessary file reads
        if self._last_read_tick > tick:
            return []

        if self.end_of_tick_entries:
            return []

        prev = self._trace_file.tell()
        prev_global_id = Commit.global_counter
        commits = []

        l = self._trace_file.readline()
        while l:
            try:
                c = Commit.parseTrace(l)
            except ValueError:
                self.end_of_tick_entries = True
                break

            # If the read commit's tick is greater than the tick we are
            # looking for, rollback the file pointer to the begin of the line.
            if c.tick > tick:
                self._trace_file.seek(prev)
                Commit.global_counter = prev_global_id
                self._last_read_tick = c.tick
                break

            if c.tick < tick:
                raise RuntimeError('Commits must be read in-order')

            commits.append(c)
            prev = self._trace_file.tell()
            prev_global_id = Commit.global_counter
            l = self._trace_file.readline()

        return commits

    # Parse trace last tick
    def parseLastTick(self):
        with open(self._trace_path, 'r') as f:
            l = f.readline()
            words = l.split()
            return int(words[-1])

    def getTickOfCommit(self, commit_no):
        self._seekLine(commit_no+1)
        l = self._trace_file.readline()
        words = l.split()
        return int(words[0])

class LittleCommitTracer(Tracer):

    def __init__(self, f):
        super().__init__(f)

    def countCommitsAtTick(self, tick):
        # Avoid unnecessary file reads
        if self._last_read_tick > tick:
            return 0

        if self.end_of_tick_entries:
            return 0

        count = 0
        prev = self._trace_file.tell()
        l = self._trace_file.readline()
        while l:
            try:
                line_tick = int(l)
            except ValueError:
                self.end_of_tick_entries = True
                break

            # If the read commits's tick is greater than the tick we are
            # looking for, rollback the file pointer to the begin of the line.
            if line_tick > tick:
                self._trace_file.seek(prev)
                self._last_read_tick = line_tick
                break

            if line_tick < tick:
                raise RuntimeError('Little commits must be read in-order')

            count += 1
            prev = self._trace_file.tell()
            l = self._trace_file.readline()

        return count

    def getTickOfCommit(self, commit_no):
        self._seekLine(commit_no)
        l = self._trace_file.readline()
        words = l.split()
        return int(words[0])

    def parseLastTick(self):
        return 0

    def _seekBeginOfTicks(self):
        super()._seekBeginOfTicks()

class IssueTracer(Tracer):

    def __init__(self, f):
        super().__init__(f)
        self._repeat_little = False

    def _parseIssue(self, line):
        words = line.split()
        return {'tick': int(words[0]),
                'fu': int(words[1]),
                'op': int(words[2]),
                'opDesc': lookup_op_desc(int(words[2]))}

    def getIssuesAtTick(self, tick):
        # Avoid unnecessary file reads
        if self._last_read_tick > tick:
            return []

        if self.end_of_tick_entries:
            return []

        issues = []
        prev = self._trace_file.tell()
        l = self._trace_file.readline()
        while l:
            try:
                i = self._parseIssue(l)
            except ValueError:
                self.end_of_tick_entries = True
                break

            # If the read issue's tick is greater than the tick we are
            # looking for, rollback the file pointer to the begin of the line.
            if i['tick'] > tick:
                self._trace_file.seek(prev)
                self._last_read_tick = i['tick']
                break

            if i['tick'] < tick:
                raise RuntimeError('Issues must be read in-order')

            # Push parsed issue into little issue buffer
            issues.append(i)
            prev = self._trace_file.tell()
            l = self._trace_file.readline()

        return issues

    def parseLastTick(self):
        last_line = None
        with open(self._trace_path, 'rb') as f:
            f.seek(-2, os.SEEK_END)
            while f.read(1) != b'\n':
                f.seek(-2, os.SEEK_CUR)
            last_line = f.readline().decode()

        last_tick = int(last_line.split()[-1])
        return last_tick

    def _seekBeginOfTicks(self):
        super()._seekBeginOfTicks()
