import re

hex_parser = re.compile(r"[\da-f]*")
# A ARM register can be named by r+number or by the ABI
# name (e.g.: ip and fp).
arm_reg_re = r"(r(\d)|sl|fp|ip|sp|lr)+,?"
neon_reg_re = r"(s|d|q)(\d)+(\[\d\])?,?"

def is_arm_reg(operand):
    return re.fullmatch(arm_reg_re, operand) is not None

def is_neon_reg(operand):
    return re.fullmatch(neon_reg_re, operand) is not None

# gem5 emmits ldrd.w and strd.w instructions for ldrd and strd.
loads_re = r"ldr|ldrd|ldrb|ldrh|ldrl|ldrex"
stores_re = r"str|strd|strb|strh|strl|strex"
neon_loads_re = r"vldr"
neon_stores_re = r"vstr"

# gem5 emmits ldr_uop and str_uop for many instructions, such as push, pops,
# stmia, ldmdb and others. These instructions usually store/load multiple regi-
# ters at once. It is a nice abstraction to handle them only as uOperations.
load_uops_re = r"ldr_uop|ldr2_uop"
store_uops_re = r"str_uop|str_uop.w|str2_uop|str_uopcs"
neon_load_uops_re = r"ldrneon8_uop|ldrneon16_uop"
neon_store_uops_re = r"strneon8_uop|strneon16_uop|strfp_uop"

# These instructions are not used to access NEON and VFP registers. vmov and
# vmrs/vmsr are used instead.
coprocessor_re = r"mrc"

# Misc instructions that can be found in a program.
mov_re = r"mov|movt|movs|mvn"
misc_re = mov_re

# Condition Codes.
cc_re = r"(eq|ne|cs|hs|cc|lo|mi|pl|vs|vchi|ls|ge|lt|gt|le|al)?"

neon_loads_re = neon_loads_re + "|" + neon_load_uops_re
neon_stores_re = neon_stores_re + "|" + neon_store_uops_re

loads_parser = re.compile("("+loads_re+"|"+load_uops_re+")"+cc_re+"(.w)?")
stores_parser = re.compile("("+stores_re+"|"+store_uops_re+")"+cc_re+"(.w)?")
neon_loads_parser = re.compile("("+neon_loads_re+")"+cc_re+"(.w)?")
neon_stores_parser = re.compile("("+neon_stores_re+")"+cc_re+"(.w)?")
coprocessor_parser = re.compile("("+coprocessor_re+")"+cc_re+"(.w)?")
misc_parser = re.compile("("+misc_re+")"+cc_re+"(.w)?")
