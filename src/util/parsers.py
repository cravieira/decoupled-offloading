import os
import re
import sys

from .commit import Commit
from .dump import *
from .re import *

def parse_line(line: str):
    """Parse trace file line

    :arg1: Line from the trace file
    :returns: Returns an Commit entry if the line is from a Commit or an int if
            it is the "Last Tick" line

    """
    words = line.split()
    entry = None
    is_commit_line = re.compile(r'(\d)+')
    try:
        if is_commit_line.fullmatch(words[0]) != None:
            tick = int(words[0])
            pc = int(words[1], 16)
            paddr = int(words[2], 16)
            bytes_range = int(words[3])
            op_class = int(words[4])
            inst = words[5:]
            entry = Commit(tick, pc, paddr, bytes_range, op_class, inst=inst)
            return entry
        elif "exiting with last active thread context" in line:
            return int(words[-1])
        else:
            return None
    except (ValueError, IndexError):
        return None

def parseLastTick(sim_file):
    last_line = None
    with open(sim_file, 'rb') as f:
        f.seek(-2, os.SEEK_END)
        while f.read(1) != b'\n':
            f.seek(-2, os.SEEK_CUR)
        last_line = f.readline().decode()

    last_tick = int(last_line.split()[-1])
    return last_tick

def findViolations(gem5_trace: str, d_file: str):
    """Find instruction violations in gem5 trace

    This function receives a path to a gem5 trace and a path to the dump file
    of the binary used in the gem5 simulation. The function looks for
    instructions that require syncronization, also named as instruction
    violations. This functions prints a new trace file representing the gem5
    simulation to the console output.

    :arg1: Path to gem5 simulation trace
    :arg2: Path to the dump file of the binary used in simulation

    """
    # List of instructions that may be instruction violations
    insts_of_interest = r"(vmov|vdup|mrc|mcr|mrrc|mcrr|vmrs)"
    dump = DumpFile(d_file, insts_of_interest)

    last_tick = parseLastTick(gem5_trace)
    with open(gem5_trace, 'r') as f:
        print('Creating trace...', file=sys.stderr)
        f.seek(0)
        Commit.global_counter = 0
        print('Last Tick:', last_tick)
        for l in f:
            e = parse_line(l)
            if type(e) is Commit:
                mark_inst_violations([e], dump)
                e.dumpTrace()

    return
