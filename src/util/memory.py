import sys
import numpy as np
from operator import itemgetter

from .commit import Commit

def find_memory_violations(paddr, access_type, ID):
        """TODO: Docstring for function.

        :function: TODO
        :returns: TODO

        """
        load_val = 0
        store_val = 1
        vstore_val = 2
        current_address = None
        violations = 0
        i = 0
        while i < len(paddr):
            if current_address is None or current_address > paddr[i]:
                current_address = paddr[i]
                vstore = False
                prev_commit = None

            if access_type[i] == vstore_val:
                vstore = True
                prev_commit = ID[i]
            if access_type[i] == store_val:
                vstore = False
                prev_commit = None
            if access_type[i] == load_val and vstore:
                violations +=1

            i += 1

        violations = np.zeros((violations, 2), dtype=np.uint64)
        current_address = None
        i = 0
        mem_ind = 0
        while i < len(paddr):
            if current_address is None or current_address > paddr[i]:
                current_address = paddr[i]
                vstore = False
                prev_commit = None

            if access_type[i] == vstore_val:
                vstore = True
                prev_commit = ID[i]
            if access_type[i] == store_val:
                vstore = False
                prev_commit = None
            if access_type[i] == load_val and vstore:
                violations[mem_ind,0] = prev_commit
                violations[mem_ind,1] = ID[i]
                mem_ind += 1

            i += 1

        return violations

class MemTrace:

    """Docstring for MemTrace. """

    def __init__(self, commits):
        print("Creating trace...", file=sys.stderr)
        self.trace = self.createTrace(commits)
        print("Finding violations...", file=sys.stderr)
        self.mem_violations = self.findMemoryViolations(self.trace)

    def createTrace(self, commits):
        # List all memory bytes accessed in a list of tuples (addr, commit)
        mem_accesses = []
        for c in commits:
            if c.isMemAccess():
                base = c.paddr
                bytes_range = c.bytes_range
                for i in range(bytes_range):
                    mem_accesses.append((base+i, c))

        # Sort list memory addresses
        mem_accesses.sort(key=itemgetter(0), reverse =True)

        # Coalesce addresses and create a list of commits: (Address, [commits])
        mem = []
        base = None
        base_commits = []
        for a in mem_accesses:
            if base is None:
                base = a[0]

            if base == a[0]:
                base_commits.append(a[1])
            elif base > a[0]:
                mem.append((base, base_commits))
                base = a[0]
                base_commits = []
                base_commits.append(a[1])
            else:
                RuntimeError("Failed to create memory trace.")
        # Append last iteration
        mem.append((base, base_commits))

        return mem

    def findMemoryViolations(self, trace):
        """TODO: Docstring for function.

        :function: TODO
        :returns: TODO

        """
        violations = []
        for entry in trace:
            vstore = False
            prev_commit = None
            for commit in entry[1]:
                if commit.vstore:
                    vstore = True
                    prev_commit = commit
                if commit.store:
                    vstore = False
                    prev_commit = None
                if commit.load and vstore:
                    violations.append(MemoryViolation((prev_commit, commit)))
        return violations

    def printMemoryViolations(self, file=sys.stdout):
        for v in self.mem_violations:
            v.print(file)

class MemoryViolation:
    """docstring for Violation"""
    _id = 0

    def __init__(self, commits, mem_v_id=None):
        self.commits = commits
        self.latency = commits[1].tick - commits[0].tick
        if mem_v_id is None:
            commits[0].markMemViolation(MemoryViolation._id)
            commits[1].markMemViolation(MemoryViolation._id)
            self._id = MemoryViolation._id
            MemoryViolation._id += 1
        else:
            self.id = mem_v_id

    @classmethod
    def parsePrint(cls, line, commits_list):
        """TODO: Docstring for parseLines.

        :function: TODO
        :returns: TODO

        """
        words = line.split()
        mem_v_id = int(words[0])
        c0_id = int(words[2])
        c1_id = int(words[4])
        commits = (commits_list[c0_id], commits_list[c1_id])
        return cls(commits, mem_v_id)
        #commits = []
        #for line in lines:
        #    words = line.split()
        #    tick = int(words[2])
        #    pc = int(words[4][2:], 16)
        #    paddr = int(words[6][2:], 16)
        #    bytes_range = int(words[8])
        #    inst = words[10:]
        #    c = Commit(tick, pc, paddr, bytes_range, inst)
        #    commits.append(c)

        return cls(commits)

    def print(self, file=sys.stdout):
        """TODO: Docstring for print.

        :function: TODO
        :returns: TODO

        """
        #print("C1: ", end="")
        #self.commits[0].print(file=file)
        #print("C2: ", end="")
        #self.commits[1].print(file=file)
        print(self._id, "C0:", self.commits[0].id, "C1:", self.commits[1].id)

