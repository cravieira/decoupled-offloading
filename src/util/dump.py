import sys

from .commit import *
from .re import is_arm_reg, is_neon_reg

def mark_inst_violations(trace, dump):
    # Run over gem5 trace and get only the commits of interest according to
    # the instructions of interest
    coi = dump.matchCoI(trace)
    coi = _filterCoI(coi)
    return coi

def _filterCoI(coi):
    """docstring for _filterCoI"""
    new_coi = []
    for commit, dump in coi:
        insert_inst = True
        try:
            # vmov is a heavily overloaded mnemonic. It can be used to re-
            # present many types of moves, such as those listed below. I
            # preferred to handle each move case and apply multiple checks
            # to some of them in order to avoid bugs.
            # Neon <- Neon
            # Neon <- Arm, Arm
            # Neon <- Imm
            # Arm, Arm <- Neon
            # Arm <- Neon[]
            if "vmov" in commit.inst:
                # Filter vmov with immediates. Their assembly is composed
                # of: Inst, NEON reg, hex(imm).
                if len(commit.asm) == 3 and \
                        re.match(r"#[\da-f]*", commit.asm[2]) != None :
                    continue
                # Test if the instruction moves to a neon register.
                elif is_neon_reg(dump.operands[0]):
                    continue
                # Test if it is a move from a neon GPR to two arm registers.
                elif is_arm_reg(dump.operands[0]) and\
                        is_arm_reg(dump.operands[1]) and\
                        is_neon_reg(dump.operands[2]):
                    pass
                # Test if it is a move from a neon scalar to an arm register.
                elif is_arm_reg(dump.operands[0]) and\
                        is_neon_reg(dump.operands[1]):
                            pass
                # If we couldn't match, then raise an exception to help im-
                # plementing a new type of vmov.
                else:
                    commit.print(sys.stderr)
                    dump.print(sys.stderr)
                    raise RuntimeError("Unexpected type of vmov")

            # This instruction is used to move the value of an extension system
            # register to a general-purpose register.
            # TODO: These checks in vmrs instruction seems to be useless. Could
            # it be removed?
            elif "vmrs" in commit.inst:
                # If the system register is fpscr, then it is possible that the
                # target register can be APSR.
                if "APSR_nzcv" in dump.operands[0] or \
                        "fpscr" in dump.operands[1]:
                    pass
                # This if expects moves from fpscr to general-purpose registers
                elif "fpscr" in dump.operands[1]:
                    pass
                else:
                    commit.print(file=sys.stderr)
                    dump.print(file=sys.stderr)
                    raise RuntimeError("Unexpected type of vmrs")

            # Filter accesses to tpiduro
            elif commit.asm[2] == "tpidruro":
                continue

            # If the instruction could reach here, then it should stay in
            # our new Commits of Interest list.
            new_coi.append(InstructionViolation(commit, dump))
        except IndexError as e:
            commit.print(file=sys.stderr)
            dump.print(file=sys.stderr)
            raise e
    return new_coi

# TODO: This class is relatively new. Previously, tuples (commit, dump) were
# used instead. Removing tuples from code is a work in progress.
class InstructionViolation:

    """Contains a commit and a instruction dump of an instruction violation"""

    def __init__(self, commit, inst_dump):
        self.commit = commit
        self.commit.markInstViolation()
        self.dump = inst_dump

class DumpFile:

    """Docstring for DumpObject. """

    def __init__(self, dump_path, insts_of_interest):
        """TODO: to be defined.

        :Docstring for DumpObject.: TODO

        """
        self.dump_path = dump_path
        self.insts_of_interest = insts_of_interest
        self.ioi = []
        self.coi = []
        with open(self.dump_path) as dump:
            for line in dump:
                match = re.findall(insts_of_interest, line)
                if (match):
                    self.ioi.append(InstDump.parseDumpLine(line))

    def matchCoI(self, commits):
        tuples = []
        # Even though we have a RE pattern to find IoI, we can't use them to
        # associate gem5's commits to instructions in dumps. This happens be-
        # cause gem5's disassemble may not be the same as the one produced by
        # objdump (e.g. PUSH and POP are emitted as uops by gem5). Thus, we can
        # only trust PC comparisons.
        for commit in commits:
            for inst in self.ioi:
                if commit.pc == inst.pc:
                    tuples.append((commit, inst))
                    commit.setDumpAsm(inst.asm)
        self.coi = tuples
        return tuples

    # TODO: Deprecated?
    def printCoI(self, file=sys.stdout):
        for i in self.coi:
            print("C: ", end="")
            i.commit.print(file)
            print("D: ", end="")
            i.dump.print(file)

    # TODO: Deprecated
    def printIoI(self, file=sys.stdout):
        for i in self.ioi:
            i.print(file)

class InstDump:

    """Docstring for InstDump. """

    def __init__(self, pc, binary, asm, inst, operands):
        """TODO: to be defined.

        :Docstring for InstDump.: TODO

        """
        self.pc = pc
        self.binary = binary
        self.asm = asm
        self.inst = inst
        self.operands = operands

    @classmethod
    def parseDumpLine(cls, line):
        """TODO: Docstring for parseDumpLine.

        :function: TODO
        :returns: TODO

        """
        words = line.split()
        pc = int(words[0][0:-1], 16)
        i = 0
        if hex_parser.fullmatch(words[2]):
            i = 1
        binary = int(''.join(words[1:2+i]), 16)
        asm = words[2+i:]
        inst = words[2+i]
        operands = words[3+i:]

        return cls(pc, binary, asm, inst, operands)

    @classmethod
    def parsePrint(cls, line):
        """TODO: Docstring for parsePrint.

        :function: TODO
        :returns: TODO

        """
        words = line.split()
        pc = int(words[1][2:], 16)
        binary = int(words[2][2:], 16)
        asm = words[3:]
        inst = words[3]
        operands = words[4:]

        return cls(pc, binary, asm, inst, operands)


    def print(self, file=sys.stdout):
        print(hex(self.pc),
              hex(self.binary),
              " ".join(self.asm),
              file=file)
