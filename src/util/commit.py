import sys
import re
import ast

from .re import *

class Commit:
    """This class store commits in a log file produced by gem5"""

    global_counter = 0

    def __init__(self, tick, pc, paddr, bytes_range, op_class,
            offload_words=0,
            inst=None,
            inst_violation=False,
            mem_violation=False,
            mem_v_ids=None,
            dump_asm=None):

        # Set commit values
        self.tick = tick
        self.pc = pc
        self.paddr = paddr
        self.op_class = op_class
        self.asm = inst
        self._dump_asm = dump_asm
        self.inst = inst if inst is None else self.asm[0]

        # Find instruction type
        self.load = False
        self.store = False
        self.vload = False
        self.vstore = False
        self.misc = False

        if loads_parser.fullmatch(self.inst):
            self.load = True
        elif stores_parser.fullmatch(self.inst):
            self.store = True
        elif neon_loads_parser.fullmatch(self.inst):
            self.vload = True
        elif neon_stores_parser.fullmatch(self.inst):
            self.vstore = True
        else:
            self.misc = True

        # Gem5 may set a trash value for the range field with non-memory
        # instructions
        self.bytes_range = 0 if not self.isMemAccess() else bytes_range

        # TODO: This check is losing importance since the exception never
        # raised. Maybe remove it?
        # Check if this instruction accesses memory
        match = self.isMemAccess()
        if self.isMemAccess():
            self.misc = True
            # Sanity check: Are we missing any load/store instructions?
            if "ld" in self.inst[0:1] or "st" in self.inst[0:1] or \
               "vld" in self.inst[0:2] or "vst" in self.inst[0:2]:
                raise RuntimeError("Unknow memory instruction \"", self.inst,
                                   "\" at address: ", hex(self.pc))

        # Check if this commit is a Neon instruction
        if self.inst[0] == "v" or self.vload or self.vstore:
            self._is_neon = True
        else:
            self._is_neon = False

        self._inst_violation = inst_violation
        self._mem_violation = mem_violation
        self.mem_v_ids = [] if mem_v_ids is None else mem_v_ids

        # This attribute defines the number of words that are necessary to
        # offload this commit. By default this value is 0 to all commits
        # except for those that do need to offload any data. The assignment
        # of a different value is made when setDumpAsm() is called.
        self.offload_words = offload_words

        self.id = Commit.global_counter
        Commit.global_counter += 1

    @classmethod
    def parsePrint(cls, line):
        """TODO: Docstring for parsePrint.

        :function: TODO
        :returns: TODO

        """
        # TODO: Commit.offload_words is not printed and not parsed. Commit
        # parsePrint() does not work as intended
        words = line.split()
        tick = int(words[1])
        pc = int(words[3][2:], 16)
        paddr = int(words[5][2:], 16)
        bytes_range = int(words[7])
        op_class = int(words[9])
        inst_v = ast.literal_eval(words[11])
        mem_v = ast.literal_eval(words[13])
        mem_v_ids = ast.literal_eval(words[15])

        dump_index = words.index("dump_asm:")
        inst = words[17:dump_index]
        dump_asm = words[dump_index+1:]
        if dump_asm == "None":
            dump_asm = None
        return cls(tick, pc, paddr, bytes_range, op_class,
                   inst=inst,
                   inst_violation=inst_v,
                   mem_violation=mem_v,
                   mem_v_ids=mem_v_ids,
                   dump_asm=dump_asm)

    def print(self, file=sys.stdout):
        dump_asm = self._dump_asm
        if self._dump_asm is not None:
            dump_asm = " ".join(self._dump_asm)

        print("Tick:", self.tick,
              "PC:", hex(self.pc),
              "paddr:", hex(self.paddr),
              "range:", self.bytes_range,
              "op_class:", self.op_class,
              "inst_v:", self._inst_violation,
              "mem_v:", self._mem_violation,
              "mem_v_ids:", str(self.mem_v_ids).replace(" ", ""),
              "gem5_asm:", " ".join(self.asm),
              "dump_asm:", self._dump_asm,
              file=file)

    @classmethod
    def parseTrace(cls, line):
        """TODO: Docstring for parsePrint.

        :function: TODO
        :returns: TODO

        """
        words = line.split()
        tick = int(words[0])
        pc = int(words[1], 16)
        paddr = int(words[2], 16)
        bytes_range = int(words[3])
        op_class = int(words[4])
        offload_words = int(words[5])
        inst_v = ast.literal_eval(words[6])
        mem_v = ast.literal_eval(words[7])
        mem_v_ids = ast.literal_eval(words[8])
        inst = ast.literal_eval(words[9])
        dump_asm = None
        return cls(tick, pc, paddr, bytes_range, op_class,
                   offload_words=offload_words,
                   inst=inst,
                   inst_violation=inst_v,
                   mem_violation=mem_v,
                   mem_v_ids=mem_v_ids,
                   dump_asm=dump_asm)


    def dumpTrace(self, file=sys.stdout):
        dump_asm = self._dump_asm
        if self._dump_asm is not None:
            dump_asm = " ".join(self._dump_asm)

        print(self.tick,
              hex(self.pc),
              hex(self.paddr),
              self.bytes_range,
              self.op_class,
              self.offload_words,
              self._inst_violation,
              self._mem_violation,
              str(self.mem_v_ids).replace(" ", ""),
              str(self.asm).replace(' ', ''),
              file=file)

    def isMemAccess(self):
        return self.load or self.store or self.vload or self.vstore

    def isNeon(self):
        return self._is_neon

    def isViolation(self):
        return self._inst_violation or self._mem_violation

    def isMemViolation(self):
        return self._mem_violation

    def markMemViolation(self, mem_v_id=None):
        self._mem_violation = True
        if mem_v_id is not None:
            self.mem_v_ids.append(mem_v_id)

    def isInstViolation(self):
        return self._inst_violation

    def markInstViolation(self):
        self._inst_violation = True

    def getDataWords(self):
        if 'vmov' in self.inst:
            # Make sure we have not only gem5's assembly but also objdump's.
            if 'vmov' not in self._dump_asm[0]:
                raise RuntimeError("Assembly from gem5 and objdump are\
                        different.")

            data = 0
            # Is the destination a NEON register?
            if is_neon_reg(self._dump_asm[1]):
                # Iterate over the operands and count how many arm registers
                # are there.
                for o in self._dump_asm[2:]:
                    if is_arm_reg(o):
                        data += 1
            return data
        # This instructions moves the value of a arm register to a special NEON
        # register. Thus, only 1 word of data is moved.
        elif 'vmsr'in self.inst:
            return 1
        else:
            return 0

    def getAccessedBytes(self):
        addrs = []
        for i in range(self.bytes_range):
            addrs.append(self.paddr+i)
        return addrs

    def setDumpAsm(self, asm):
        self._dump_asm = asm
        self.offload_words = self.getDataWords()

    def __eq__(self, value):
        if self.tick != value.tick:
            return False
        elif self.pc != value.pc:
            return False
        elif self.paddr != value.paddr:
            return False
        elif self.bytes_range != value.bytes_range:
            return False
        elif self.inst != value.inst:
            return False
        else:
            return True
